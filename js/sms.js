const SMS_URL = "/crediting/api/sms_send_test";
const regex_phone = new RegExp(/^[\+\d]?(?:[\d-.\s()]*)$/);
const regex_iin = new RegExp(/^[0-9]*$/);
const PHONE = () => $("#phone").val() || "";
const IIN = () => $("#iin").val() || "";
const SMS_TIMEOUT = 50000; // 50 sec

let isSmsAvailable = true;
let smsInterval;
let tempTimer;

const send_sms = (phone, iin) => {
  let dataSend = new FormData();

  dataSend.append("phone", phone);
  dataSend.append("iin", iin);

  if (
    regex_phone.test(phone) &&
    phone.length == 11 &&
    regex_iin.test(iin) &&
    iin.length === 12 &&
    isSmsAvailable
  ) {
    console.log("requested!");
    $.ajax({
      url: SMS_URL,
      data: dataSend,
      processData: false,
      contentType: false,
      type: "POST",
      success: function (data) {
        console.log("Sent!");
        $("#fio").show();
        smsTimeoutStart(); // выключить повторную отправку на 50 секунд
        smsIntervalStart(); // начать обратный отсчет
      },
    });
  }
};

const validate_and_send = () => {
  const phone = normalizePhone(PHONE());
  const iin = IIN();

  if (
    regex_phone.test(phone) &&
    phone.length === 11 &&
    regex_iin.test(iin) &&
    iin.length === 12 &&
    isSmsAvailable
  ) {
    send_sms(phone, iin);
  }
};

// отправка смс при клике на тестовую кнопку
$("#sms_btn").on("click", function () {
  validate_and_send();
});

// SMS timeout
function smsTimeoutStart() {
  isSmsAvailable = false;
  setTimeout(() => {
    isSmsAvailable = true;
    clearInterval(smsInterval);
    $("#sms-timer").hide();
  }, SMS_TIMEOUT);
}

// SMS interval
const smsIntervalStart = () => {
  tempTimer = SMS_TIMEOUT / 1000 - 1; // Translate to ms to seconds
  $("#sms-timer").show();

  smsInterval = setInterval(() => {
    $("#sms-timer").html(`(${tempTimer})`);
    tempTimer -= 1;
  }, 1000);
};

// При вводе номер телефона или ИИН отправлять смс
$("#phone").on("input", validate_and_send);
$("#iin").on("input", validate_and_send);
$("#retry-sms").on("click", function (e) {
  e.preventDefault();
  if (isSmsAvailable) {
    validate_and_send(); // отправить смс
  } else {
    alert(`Пожалуйста, подождите еще ${tempTimer} секунд!`);
  }
});

// validations
const normalizePhone = (phone) => {
  phone = phone
    .replace("(", "")
    .replace(/\)/g, "")
    .replace(/\-/g, "")
    .replace(/\_/g, "")
    .replace(/\ /g, "")
    .replace(/\_/g, "")
    .replace(/\+/g, "");

  return phone || "";
};
