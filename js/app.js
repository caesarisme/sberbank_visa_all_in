const reasonsWrapper = $("#reasons-wrapper");
const arrowLeft = $("#arrowLeft");
const arrowRight = $("#arrowRight");
const animationDuration = 0.3 * 1000; // 300ms
const SALYK_URL = "/crediting/api/personal_data2/";
let currentReason = 1;

$(document).ready(() => {
  // Carousel arrows
  arrowRight.on("click", () => {
    if (currentReason >= $(".reasons-card").length - 1) return;
    reasonsWrapper.animate(
      {
        scrollLeft: nextCardPos(currentReason),
      },
      animationDuration
    );
    currentReason++;
  });

  arrowLeft.on("click", () => {
    if (currentReason <= 1) return;
    reasonsWrapper.animate(
      {
        scrollLeft: prevCardPos(currentReason),
      },
      animationDuration
    );
    currentReason--;
  });

  // Scroll buttons
  $("[scroll-to]").on("click", function (e) {
    e.preventDefault();
    console.log("click");
    const scrollToElement = $($(e.target).attr("scroll-to"));
    $([document.documentElement, document.body]).animate(
      {
        scrollTop: scrollToElement.offset().top,
      },
      animationDuration
    );
  });

  // Salyk
  $("#iin").on("input", async function (e) {
    const inputField = $(e.target);
    if (inputField.val().length === 12) {
      try {
        const data = await getFioByIin(inputField.val());
        console.log(data);
        $("#first_name").val(transliterate(data.first_name[0]));
        $("#last_name").val(transliterate(data.last_name[0]));
      } catch (e) {}
    }
  });
});

const scrollOffset = () => ($(window).width() - $(".container").width()) / 2;
const nextCardPos = (n) =>
  $(`.reasons-card[data-step=${n + 1}]`).offset().left -
  scrollOffset() +
  reasonsWrapper.scrollLeft() -
  ($(`.reasons-card[data-step=${n + 1}]`).outerWidth(true) -
    $(`.reasons-card[data-step=${n + 1}]`).width()) /
    2; // Да-да, много математики, все вручную)

const prevCardPos = (n) =>
  reasonsWrapper.scrollLeft() +
  $(`.reasons-card[data-step=${n - 1}]`).offset().left -
  scrollOffset() -
  ($(`.reasons-card[data-step=${n + 1}]`).outerWidth(true) -
    $(`.reasons-card[data-step=${n + 1}]`).width()) /
    2;

const getFioByIin = async (iin) => {
  try {
    const { data: response } = await axios.get(`${SALYK_URL}${iin}`);

    if (!response.first_name) return;

    return response;
  } catch (e) {
    throw new Error("Failed to reach Salyk");
  }
};

const transliterate = (line) => {
  let answer = "",
    a = {};

  a["Ё"] = "YO";
  a["Й"] = "I";
  a["Ц"] = "Ts";
  a["У"] = "U";
  a["К"] = "K";
  a["Е"] = "E";
  a["Н"] = "N";
  a["Г"] = "G";
  a["Ш"] = "SH";
  a["Щ"] = "SCH";
  a["З"] = "Z";
  a["Х"] = "H";
  a["Ъ"] = "'";
  a["ё"] = "yo";
  a["й"] = "i";
  a["ц"] = "ts";
  a["у"] = "u";
  a["к"] = "k";
  a["е"] = "e";
  a["н"] = "n";
  a["г"] = "g";
  a["ш"] = "sh";
  a["щ"] = "sch";
  a["з"] = "z";
  a["х"] = "h";
  a["ъ"] = "'";
  a["Ф"] = "F";
  a["Ы"] = "Y";
  a["В"] = "V";
  a["А"] = "A";
  a["П"] = "P";
  a["Р"] = "R";
  a["О"] = "O";
  a["Л"] = "L";
  a["Д"] = "D";
  a["Ж"] = "ZH";
  a["Э"] = "E";
  a["ф"] = "f";
  a["ы"] = "i";
  a["в"] = "v";
  a["а"] = "a";
  a["п"] = "p";
  a["р"] = "r";
  a["о"] = "o";
  a["л"] = "l";
  a["д"] = "d";
  a["ж"] = "zh";
  a["э"] = "e";
  a["Я"] = "YA";
  a["Ч"] = "CH";
  a["С"] = "S";
  a["М"] = "M";
  a["И"] = "I";
  a["Т"] = "T";
  a["Ь"] = "'";
  a["Б"] = "B";
  a["Ю"] = "YU";
  a["я"] = "ya";
  a["ч"] = "ch";
  a["с"] = "s";
  a["м"] = "m";
  a["и"] = "i";
  a["т"] = "t";
  a["ь"] = "'";
  a["б"] = "b";
  a["ю"] = "yu";
  a["Қ"] = "K";
  a["Ә"] = "A";
  a["І"] = "I";
  a["Ң"] = "N";
  a["Ү"] = "U";
  a["Ұ"] = "U";
  a["Ө"] = "O";
  a["Һ"] = "KH";
  a["қ"] = "k"; // ?
  a["ә"] = "a";
  a["і"] = "i";
  a["ң"] = "n";
  a["ү"] = "u";
  a["ұ"] = "u";
  a["ө"] = "o";
  a["һ"] = "kh";

  for (i in line) {
    if (line.hasOwnProperty(i)) {
      if (a[line[i]] === undefined) {
        answer += line[i];
      } else {
        answer += a[line[i]];
      }
    }
  }
  return answer;
};
